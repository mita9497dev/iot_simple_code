#include <SoftwareSerial.h>

#define RX 12 // D6
#define TX 13 // D7
SoftwareSerial hc05Serial(RX, TX);

//#define hc05 Serial // Test
#define hc05 hc05Serial
#define CMD_LEN 128
char cmd[CMD_LEN];
int currentLen = 0;

/* =============================================
    HƯỚNG DẪN
    - Kết nối chân của hc05 với nodemcu
    #define RX 12 // D6
    #define TX 13 // D7

    hoặc
    #define RX D6
    #define TX D7
    
    -
    Ví dụ: Muốn tắt chân D5, bật chân D6, bật chân D7 
    Chuỗi gửi qua bluetooth có dạng: 
    d5_off$d6_on$d7_on
    HOẶC:
    d5_offd6_ond7_on

    -
    DÙNG CHÂN NÀO NHỚ PINMODE CHÂN ĐÓ TRONG SETUP
    pinMode(D5, OUTPUT);
    pinMode(D6, OUTPUT);
    pinMode(D7, OUTPUT);
================================================ */

void setup()
{
    Serial.begin(115200);
    delay(1000);

    hc05Serial.begin(115200);
    delay(1000);

    pinMode(D5, OUTPUT);
    pinMode(D6, OUTPUT);
    pinMode(D7, OUTPUT);
}

void loop()
{
    if (HC05_Listen())
    {
        HC05_ProcessCMD();
    }
}

// Đọc dữ liệu từ HC05
bool HC05_Listen()
{
    if (hc05.available())
    {
        // Reset chuỗi cmd
        cmd[0] = '\0';
        currentLen = 0;
        
        while(hc05.available())
        {
            char c = hc05.read();
            Serial.print(c);
            // Bỏ qua ký tự null, kết thúc dòng, xuống dòng
            if (c != '\0' && c != '\r' && c != '\n' && currentLen + 1 < CMD_LEN)
            {
                cmd[currentLen++] = c;
                cmd[currentLen] = '\0';
            }
            
            ESP_Yeild();
        }

        Serial.print("cmd: ");
        Serial.println(cmd);
    }
}

void HC05_ProcessCMD()
{
    if (strstr((const char*)cmd, "d5_on") != nullptr)
    {
        // D5 on
        controlPin(D5, HIGH);
    }
    else if (strstr((const char*)cmd, "d5_off") != nullptr)
    {
        // D5 off
        controlPin(D5, LOW);
    }
    
    if (strstr((const char*)cmd, "d6_on") != nullptr)
    {
        // D6 on
        controlPin(D6, HIGH);
    }
    else if (strstr((const char*)cmd, "d6_off") != nullptr)
    {
        // D6 off
        controlPin(D6, LOW);
    }

    if (strstr((const char*)cmd, "d7_on") != nullptr)
    {
        // D7 on
        controlPin(D7, HIGH);
    }
    else if (strstr((const char*)cmd, "d7_off") != nullptr)
    {
        // D7 off
        controlPin(D7, LOW);
    }

    // Reset chuỗi cmd
    cmd[0] = '\0';
    currentLen = 0;
}

void controlPin(uint8_t pin, uint8_t value)
{
    Serial.print("pin: ");
    Serial.println(pin);
    Serial.print("value: ");
    Serial.println(value);

    digitalWrite(pin, value);
}

void ESP_Yeild()
{
    delay(1);
}
