#include <SoftwareSerial.h>

#define RX 12 // D6
#define TX 13 // D7
SoftwareSerial hc05Serial(RX, TX);

#define hc05 Serial // Test
//#define hc05 hc05Serial
#define CMD_LEN 128
char cmd[CMD_LEN];
int currentLen = 0;

/* =============================================
    HƯỚNG DẪN
    - Kết nối chân của hc05 với nodemcu
    #define RX 12 // D6
    #define TX 13 // D7
    
    -
    Ví dụ: Muốn bật chân D1, tắt chân D2, bật chân D8 
    Chuỗi gửi qua bluetooth có dạng: 
    1_1$2_0$15_1$

    -
    DÙNG CHÂN NÀO NHỚ PINMODE CHÂN ĐÓ TRONG SETUP
    pinMode(D7, OUTPUT);
    pinMode(D8, OUTPUT);
================================================ */

void setup()
{
    Serial.begin(115200);
    delay(1000);

    hc05Serial.begin(115200);
    delay(1000);

    pinMode(D7, OUTPUT);
    pinMode(D8, OUTPUT);
}

void loop()
{
    if (HC05_Listen())
    {
        HC05_ProcessCMD();
    }
}

bool HC05_Listen()
{
    if (hc05.available())
    {
        // Reset chuỗi cmd
        cmd[0] = '\0';
        currentLen = 0;
        
        while(hc05.available())
        {
            char c = hc05.read();
            Serial.print(c);

            if (c != '\0' && c != '\r' && c != '\n' && currentLen + 1 < CMD_LEN)
            {
                cmd[currentLen++] = c;
                cmd[currentLen] = '\0';
            }
            
            ESP_Yeild();
        }

        Serial.print("cmd: ");
        Serial.println(cmd);
    }
}

void HC05_ProcessCMD()
{
    char* cmdPtr = cmd;
    while (cmdPtr != '\0')
    {
        char* undercore = strchr((const char*)cmdPtr, '_');
        if (undercore == nullptr)
        {
            break;
        }        
        char* p = cmdPtr;
        *undercore++ = '\0';
        cmdPtr = undercore;

        uint8_t pin = atoi(p);

        char* dollar = strchr((const char*)cmdPtr, '$');
        if (dollar == nullptr)
        {
            break;
        }
        p = cmdPtr;
        *dollar++ = '\0';
        cmdPtr = dollar;

        uint8_t value = atoi(p);

        controlPin(pin, value);

        ESP_Yeild();
    }

    cmd[0] = '\0';
    currentLen = 0;
}

void controlPin(uint8_t pin, uint8_t value)
{
    Serial.print("pin: ");
    Serial.println(pin);
    Serial.print("value: ");
    Serial.println(value);

    digitalWrite(pin, value);
}

void ESP_Yeild()
{
    delay(1);
}
