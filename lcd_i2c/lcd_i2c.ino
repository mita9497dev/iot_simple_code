#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

// Chân lcd sử dụng là:
// SDA --> A4
// SCL --> A5
LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display


#include "DHT.h"

// DHT11 sử dụng chân D2
#define DHTPIN 2
#define DHTTYPE DHT11

// KHAI BÁO DHT 11
DHT dht(DHTPIN, DHTTYPE);

void setup()
{

    Serial.begin(9600);

    // Cài đặt DHT11
    dht.begin();

    // Cài đặt LCD
    lcd.init();
    // Print a message to the LCD.
    lcd.backlight();
    lcd.setCursor(1,0);
    lcd.print("hello everyone");
    lcd.setCursor(1,1);
    lcd.print("MiTa Ebi");
}


void loop()
{
}
